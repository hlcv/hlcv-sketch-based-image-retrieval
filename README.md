# Sketch based image retrieval - Tool

![Screenshot](img/screenshot.png)

## Building

## On Unix:

```bash
git clone ...
cd <directory>
mkdir build
cd build
cmake  ..
make -j8
./src/hlcvproject
```

### Others

You are on your own.

## Acknowledgements

This project use some code from DigiKam, an amazing Photo Management Tools. You
should check it out.

## License

This code is licensed under the same license has DigiKam: GPL2 or later
