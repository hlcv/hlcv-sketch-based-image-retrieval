set(hlcvproject_SRCS
    main.cpp
    hlcvprojectwindow.cpp
)

ecm_qt_declare_logging_category(hlcvproject_SRCS
    HEADER hlcvprojectdebug.h
    IDENTIFIER HLCVPROJECT
    CATEGORY_NAME "hlcvproject"
)
ki18n_wrap_ui(hlcvproject_SRCS
    hlcvprojectview.ui
)
add_subdirectory(fuzzysearch)

add_executable(hlcvproject ${hlcvproject_SRCS})

target_link_libraries(hlcvproject
    KF5::CoreAddons
    KF5::I18n
    KF5::XmlGui
    KF5::ConfigWidgets
    KF5::DBusAddons
    KF5::Crash
    Qt5::Widgets
    fuzzysearch_src
)

install(TARGETS hlcvproject ${KDE_INSTALL_TARGETS_DEFAULT_ARGS})
install(PROGRAMS org.example.hlcvproject.desktop  DESTINATION ${KDE_INSTALL_APPDIR})
install(FILES org.example.hlcvproject.appdata.xml DESTINATION ${KDE_INSTALL_METAINFODIR})
install(FILES hlcvprojectui.rc DESTINATION ${KDE_INSTALL_KXMLGUI5DIR}/hlcvproject)
