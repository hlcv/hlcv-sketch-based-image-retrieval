/*
Copyright (C) 2019 by Carl Schwan <carl@carlschwan.eu>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of
the License or (at your option) version 3 or any later version
accepted by the membership of KDE e.V. (or its successor approved
by the membership of KDE e.V.), which shall act as a proxy 
defined in Section 14 of version 3 of the license.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// application header
#include "hlcvprojectwindow.h"
#include "hlcvprojectdebug.h"

// KF headers
#include <KCrash>
#include <KDBusService>
#include <KAboutData>
#include <KLocalizedString>

// Qt headers
#include <QApplication>
#include <QCommandLineParser>
#include <QIcon>
#include <QLoggingCategory>


int main(int argc, char **argv)
{
    QApplication application(argc, argv);

    KLocalizedString::setApplicationDomain("hlcvproject");
    KCrash::initialize();

    KAboutData aboutData( QStringLiteral("hlcvproject"),
                          i18n("hlcvproject"),
                          QStringLiteral("0.1"),
                          i18n("Sketch based image retrieval"),
                          KAboutLicense::GPL,
                          i18n("Copyright 2019, Carl Schwan <carl@carlschwan.eu>"));

    aboutData.addAuthor(i18n("Carl Schwan"),i18n("Author"), QStringLiteral("carl@carlschwan.eu"));
    aboutData.setOrganizationDomain("example.org");
    aboutData.setDesktopFileName(QStringLiteral("org.example.hlcvproject"));

    KAboutData::setApplicationData(aboutData);
    application.setWindowIcon(QIcon::fromTheme(QStringLiteral("hlcvproject")));

    QCommandLineParser parser;
    aboutData.setupCommandLine(&parser);

    parser.process(application);
    aboutData.processCommandLine(&parser);

    KDBusService appDBusService(KDBusService::Multiple | KDBusService::NoExitOnFailure);

    hlcvprojectWindow *window = new hlcvprojectWindow;
    window->show();

    return application.exec();
}
